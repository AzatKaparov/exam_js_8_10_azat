import {Route, Switch, BrowserRouter} from "react-router-dom";
import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css';
import Quotes from "./containers/Quotes";
import AddQuote from "./containers/AddQuote";


function App() {
  return (
      <div className="app">
        <BrowserRouter>
          <Switch>
            <Route path="/" exact component={Quotes}/>
            <Route path="/quotes/:category" exact component={Quotes}/>
            <Route path="/quote/add" component={AddQuote}/>
            <Route path="/quote/:id/edit" exact component={AddQuote}/>
          </Switch>
        </BrowserRouter>
      </div>
  );
}

export default App;