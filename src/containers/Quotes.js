import React, {useEffect, useState} from 'react';
import './styles/Quotes.css';
import MyNavbar from "../components/UI/MyNavbar";
import CategoriesMenu from "../components/CategoriesMenu";
import QuoteItem from "../components/QuoteItem";
import axiosQuotes from "../axios-quotes";
import Preloader from "../components/UI/Preloader";

const Quotes = ({match}) => {
    let paramsCategory;
    if (match.params.category) {
        paramsCategory = match.params.category;
    }
    const [quotes, setQuotes] = useState([]);
    const [isLoading, setIsLoading] = useState(false);

    const getNewArray = initialData => {
        const newArray = [];
        Object.keys(initialData).forEach(key => {
            const item = initialData[key];
            newArray.push({
                id: key,
                ...item
            });
        });
        return newArray;
    }

    const deleteQuoteHandler = (id) => {
        setIsLoading(true);
        axiosQuotes.delete(`./quotes/${id}.json`)
            .then(response => {
                if (response.status === 200) {
                    setQuotes(quotes.filter(quote => (quote.id !== id)));
                    setIsLoading(false);
                }
            });
    };

    useEffect(() => {
        setIsLoading(true);
        const fetchData = async () => {
            const postResponse = await axiosQuotes.get('./quotes.json');
            if (paramsCategory && paramsCategory !== 'all') {
                await axiosQuotes.get(`/quotes.json?orderBy="category"&equalTo="${paramsCategory}"`)
                    .then(filteredResponse => {
                        setQuotes(getNewArray(filteredResponse.data));

                    })
            } else {
                setQuotes(getNewArray(postResponse.data));
            }
            setIsLoading(false);
        }
        fetchData().catch(console.error);
    }, [paramsCategory, match.params.id]);


    return (
        <>
            {isLoading ? <Preloader/> : null}
            <div className="container">
                <MyNavbar/>
                <div className="quote-block">
                    <CategoriesMenu/>
                    <div className="quotes">
                        {quotes.length !== 0
                            ? quotes.map(quote => {
                                return (
                                    <QuoteItem
                                        id={quote.id}
                                        category={quote.category}
                                        text={quote.text}
                                        author={quote.author}
                                        key={quote.id}
                                        deleted={() => deleteQuoteHandler(quote.id)}
                                    />
                                );
                            })
                            : <h2>There's no such quotes</h2>
                        }
                    </div>
                </div>
            </div>
        </>
    );
};

export default Quotes;