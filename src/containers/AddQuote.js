import React, {useState, useEffect} from 'react';
import MyNavbar from '../components/UI/MyNavbar';
import {Form, Button} from 'react-bootstrap';
import categories from "../constants";
import axiosQuotes from "../axios-quotes";
import Preloader from "../components/UI/Preloader";

const AddQuote = ({history, match}) => {
    const [selectedOptions, setSelectedOptions] = useState({
        author: '',
        text: '',
        category: 'all',
    });
    const [isLoading, setIsLoading] = useState(false);

    useEffect(() => {
        if (match.params.id) {
            setIsLoading(true);
            const fetchData = async () => {
                const quoteResponse = await axiosQuotes.get(`./quotes/${match.params.id}.json`);
                const data = quoteResponse.data;
                setSelectedOptions({
                    author: data.author,
                    text: data.text,
                    category: data.category,
                });
                setIsLoading(false)
            };
            fetchData().catch(console.error);
        }
    }, [match.params.id]);


    const dataChanged = e => {
        const name = e.target.name;
        const value = e.target.value;

        setSelectedOptions(prevState => ({
            ...prevState,
            [name]: value
        }));
    };

    const quoteSendHandler = async e => {
        e.preventDefault();
        const quote = {
            author: selectedOptions.author,
            text: selectedOptions.text,
            category: selectedOptions.category,
        };
        setIsLoading(true);
        if (!match.params.id) {
            try {
                await axiosQuotes.post('./quotes.json', quote);
            } finally {
                setIsLoading(false);
                history.push('/');
            }
        } else {
            try {
                await axiosQuotes.put(`./quotes/${match.params.id}.json`, quote);
            } finally {
                setIsLoading(false);
                history.replace('/');
            }
        }
    };


    return (
        <>
            {isLoading ? <Preloader/> : null}
            <div className="container">
            <MyNavbar/>
            <Form onSubmit={quoteSendHandler}>
                <Form.Group className="mb-3" controlId="authorName">
                    <Form.Label>Your name</Form.Label>
                    <Form.Control
                        type="text"
                        name="author"
                        value={selectedOptions.author}
                        onChange={dataChanged}
                        placeholder="John Doe..."
                    />
                </Form.Group>
                <Form.Group>
                    <Form.Control name="category" onChange={dataChanged} as="select" aria-label="Select category">
                        {match.params.id &&
                            <option value={selectedOptions.category}>{selectedOptions.category.toLocaleUpperCase()}</option>
                        }
                        {categories.map(category => (
                            <option key={category.id}  value={category.id}>{category.title}</option>
                        ))}
                    </Form.Control>
                </Form.Group>
                <Form.Group className="mb-3" controlId="authorQuoteText">
                    <Form.Label>Quote text</Form.Label>
                    <Form.Control
                        as="textarea"
                        name="text"
                        rows={3}
                        value={selectedOptions.text}
                        onChange={dataChanged}
                    />
                </Form.Group>
                <Button type="submit">Confirm</Button>
            </Form>
        </div>
        </>
    );
};

export default AddQuote;