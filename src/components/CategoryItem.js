import React from 'react';

const CategoryItem = ({ href, title }) => {
    return (
        <li className="category-item">
            <a className="category-link" href={href}>{title}</a>
        </li>
    );
};

export default CategoryItem;