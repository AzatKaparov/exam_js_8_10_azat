import React from 'react';
import "../styles/Preloader.css";

const Preloader = () => {
    return (
        <div className="preloader">
            <h1 className="text-white text-uppercase">LOADING...</h1>
        </div>
    );
};

export default Preloader;