import React from 'react';
import {Navbar, Nav} from 'react-bootstrap';

const MyNavbar = () => {
    return (
        <Navbar className="mb-4" bg="light" expand="lg">
            <Navbar.Brand href="/">Quotes</Navbar.Brand>
            <Navbar.Toggle aria-controls="navbar-nav" />
            <Navbar.Collapse id="navbar-nav">
                <Nav className="mr-auto">
                    <Nav.Link href="/">Home</Nav.Link>
                    <Nav.Link href="/quote/add">Add</Nav.Link>
                </Nav>
            </Navbar.Collapse>
        </Navbar>
    );
};

export default MyNavbar;