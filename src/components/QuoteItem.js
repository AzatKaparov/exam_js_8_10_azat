import React from 'react';
import {Card, Button} from 'react-bootstrap';


const QuoteItem = ({ id, author, text, category, deleted }) => {
    return (
        <Card className="mb-2">
            <Card.Header>{category.toLocaleUpperCase()}</Card.Header>
            <Card.Body>
                <Card.Title>{author}</Card.Title>
                <Card.Text className="quote-text">
                    {text}
                </Card.Text>
                <Button
                    href={`/quote/${id}/edit`}
                    variant="success">
                    Edit
                </Button>
                <Button
                    onClick={deleted}
                    variant="danger"
                    className="ml-2">
                    Delete
                </Button>
            </Card.Body>
        </Card>
    );
};

export default QuoteItem;