import React from 'react';
import './styles/Categories.css';
import categories from '../constants';
import CategoryItem from "./CategoryItem";

const CategoriesMenu = () => {
    return (
        <div className="categories-menu">
            <ul className="categories-ul">
                {categories.map(category => (
                    <CategoryItem
                        href={`/quotes/${category.id}`}
                        title={category.title}
                        key={category.id}
                    />
                ))}
            </ul>
        </div>
    );
};

export default CategoriesMenu;