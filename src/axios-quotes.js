import axios from "axios";

const axiosQuotes = axios.create({
    baseURL: 'https://exam-8-209e9-default-rtdb.firebaseio.com/'
});

export default axiosQuotes;