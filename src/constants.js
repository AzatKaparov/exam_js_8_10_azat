const categories = [
    {title: "All", id: "all"},
    {title: "Anime", id: "anime"},
    {title: "Humor", id: "humor"},
    {title: "Shrek", id: "shrek"},
];

export default categories;